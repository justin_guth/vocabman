# VocabMan

## Prerequisites

### openpyxl 

`pip install openpyxl`

### genanki

`pip install genanki`

### colorama

`pip install colorama`

### readchar

`pip install readchar`

### pillow (only needed for the kanji template)

`pip install pillow`  

### markdown2

`pip install markdown2`


# Using the app

The app currently supports only the command line interface.
Simply run `py ./Main.py` to launch the app, you will then be prompted for input.

## Commands

The following commands are supported by the app:

| command                   | parameters                 | description                                                                                                                                                                                                                                          |
| ------------------------- | -------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `help`                    | None                       | Prints a list of available commands.                                                                                                                                                                                                                 |
| `cd`                      | A relative path            | Changes the current directory to the provided path.                                                                                                                                                                                                  |
| `ls`                      | None                       | Lists the contents of the current directory.                                                                                                                                                                                                         |
| `list`/`picked`           | None                       | Lists the working set of vocab sets and templates. The listed vocab set entries give information about applied templates and reversed state of the set. `[d]` means a default template is assigned and any other content names the applied template. |
| `pickall`/`takeall`       | None                       | Adds all vocab sets and templates in the current directory to the current working set.                                                                                                                                                               |
| `pick`/`take`             | A relative path            | Adds a given vocab set or template to the current working set.                                                                                                                                                                                       |
| `drop`                    | A list of numbers or `all` | Removes the vocab set at the given number from the working set or all.                                                                                                                                                                               |
| `dropt`                   | A list of numbers or `all` | Removes the template at the given number from the working set or all.                                                                                                                                                                                |
| `dump`/`pack`             | A relative path            | Writes the current working set into an anki deck.                                                                                                                                                                                                    |
| `reverse`                 | A list of numbers or `all` | Reverses the direction of a vocab set at the given number or all.                                                                                                                                                                                    |
| `template`/`link`         | Two or more numbers        | Links the template with the number of the first parameter to all vocab sets with the numbers of the following parameters.                                                                                                                            |
| `untemplate`/`unlink`     | A list of numbers or `all` | Clears templates from the vocab sets with the given numbers or all.                                                                                                                                                                                  |
| `deploy`/`launch`/`media` | The path to a folder       | Moves all contents of the folder into the Anki media resource path. (very experimental)                                                                                                                                                              |
| `dumpdeploy`/`dd`         | None                       | Dump deck and immediately deploy any resulting media folders. (very experimental)                                                                                                                                                                    |
| `quit`/`halt`/`exit`      | None                       | Take a guess.                                                                                                                                                                                                                                        |

## Vocab sets

Vocab sets are currently coded in the form of .xlsx spreadsheets.
The first cell of this spreadsheet must contain the text "`VOCAB_MANAGER_FORMAT`" followed by cells with a title, a description and a number of columns containing the actual vocab data in the first row.

The following rows should contain vocab data. Each row denotes a tuple of cell contents.
Each row represents one vocab entry.

When writing custom templates you can decide which column is to be assigned to which template macro.

Empty rows are skipped while parsing. This might make structuring a little bit easier.

## Templates

A template describes what a rendered vocab set will look like when packaging a deck.

A template is a folder that contains at least the following files:

- `back.html`
- `front.html`
- `config.json`
- `style.css`

Furthermore you may choose to include a `processor.py` file in the Template.

### `back.html` and `front.html`

These two html files describe the structure of the front- and backside of a card.
You may use macros as provided by anki such as the `{{FrontSide}}` shorthand.
Please note that if you use the `{{FrontSide}}` macro your cards wont be reversible.
Make sure to set the field `reversible` to `false` in your `config.json`

All fields set in the `config.json` will be available within the html files.

### `style.css`

Through this file you can specify how the structure is to be rendered while packaging.

### `config.json`

This file specifies other information about the template.
A basic template might look something like this:

```json
{
    "name": "Example template",
    "reversible": true,
    "fields": [
        {"name": "Question"},
        {"name": "Translation"}
    ]
}
```

The `name` field specifies the name of your template.
The `reversible` field specifies whether your template can be reversed.
If a template is reversible vocab sets can be set to show the answer and question sides in reversed order.
The array at `fields` contains the names of the columns in your vocab set.
In this example `Question` would be the name of the first column and `Translation` the name of the second.
Those names are to be used within the `front.html` and `back.html` files as macros such as `{{Question}}` and `{{Translation}}`

A default Template for 2-column formats is provided by default and is applied to all vocab sets without a linked template.

### `processor.py`

The processor file is an optional data processor for the Template.
Within the processor an entry point must be defined through the following function:

```python
def process(row, vocabManInterface):

    # do something...
```

The `process` function is given 2 parameters namely `row` and `vocabManInterface`.
The `row` variable will contain a tuple with the current row's cell contents.
The `vocabManInterface` variable will contain an interface class instance through which you may interact the application while processing data.

### The `vocabManInterface` variable

To be continued