import requests
from PIL import Image, ImageDraw
import re
import time
import hashlib

scale = 44


def drawUpTo(n, data):

    current = 0

    im = Image.new('RGB', (256, 256), (255, 255, 255))
    draw = ImageDraw.Draw(im)
    lastP = (0, 0)
    nextP = (0, 0)
    down = False

    for stroke in data["strokes"]:

        if current > n:

            return im

        nextP = (stroke[0][0] / scale,  stroke[0][1] / scale)
        if (down):
            draw.line([lastP, nextP], fill=0, width=6)
        lastP = nextP
        down = True

        for strokePList in stroke[1:]:

            for strokeP in strokePList:

                nextP = (strokeP[0] / scale,  strokeP[1] / scale)
                if (down):
                    draw.line([lastP, nextP], fill=0, width=6)
                lastP = nextP
                down = True

        current += 1
        down = False

    return im


def generateStrip(imgs):

    imgCount = len(imgs)

    cols = min(imgCount, 8)
    rows = imgCount // 8

    if imgCount % 8 != 0:
        rows += 1

    im = Image.new('RGB', (cols * 256, rows * 256), (255, 255, 255))

    for i in range(imgCount):

        img = imgs[i]

        pasteX = (i % 8) * 256
        pasteY = (i // 8) * 256

        im.paste(img, (pasteX, pasteY))

    return im


def getCharacterStrokes(character):

    URL = "https://kanji.sljfaq.org/kanjivg/memory.cgi?o=json&k=" + character

    t = True

    while t:
        try:
            r = requests.get(url=URL)
            t = False
        except:
            time.sleep(5)

    data = r.json()

    # print (data)

    imgs = []

    for i in range(data["n_strokes"]):

        im = drawUpTo(i, data)
        imgs.append(im)

    strip = generateStrip(imgs)
    return strip


def getCharacterSequenceStrokes(sequence):

    strips = []
    height = 0

    for c in sequence:

        if re.match(r'[\u4e00-\u9fff]+', c):

            strips.append(getCharacterStrokes(c))
            height += strips[-1].height

        else:

            pass
            # print("Omitting:", c)

    if (len(strips) == 0):

        return None

    final = Image.new('RGB', (8 * 256, height), (255, 255, 255))

    currentHeight = 0

    for i in range(len(strips)):

        final.paste(strips[i], (0, currentHeight))
        currentHeight += strips[i].height

    return final


def after(vocabManInterface):

    pass

def before(vocabManInterface):

    pass

def process(row, vocabManInterface, path):

    if len(row) < 2:

        return [None]

    if row[1] == None:

        return [None]

    param = row[1]

    nparam = ""

    for c in param:

        if re.match(r'[\u4e00-\u9fff]+', c):

            nparam += c

    param = nparam

    name = hashlib.md5(param.encode("utf-8")).hexdigest()

    result = None

    if vocabManInterface.isImageCached(name):

        result = vocabManInterface.getCachedImage(name)

    else:    
        
        result = getCharacterSequenceStrokes(param)

    vocabManInterface.generateImageResource(result, name)
    vocabManInterface.packageResources(f"{path}.res")
    vocabManInterface.cacheResources()
    vocabManInterface.setReplacementField("ImagePath", name + ".png")

    
