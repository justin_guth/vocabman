import os
from src.Template import Template

class AppState:

    def __init__(self):

        self.running = True
        self.pickedVocabSets = []
        self.cwd = os.getcwd()
        self.activeTemplates = []
        self.title = None
        self.virtualFields = {}
        self.replacementFields = {}
        self.imageResources = {}

    def changeDirectory(self, path):

        os.chdir(path)
        self.cwd = os.getcwd()

    def getPickedFiles(self):

        return self.pickedVocabSets

    def getSelectedTemplates(self):

        return self.activeTemplates

    def setTitle(self, title):

        self.title = title

    def isTitleSet(self):

        return self.title != None

    def getTitle(self):

        return self.title

    def getVirtualFields(self) -> dict:
    
        return self.virtualFields

    def removeVirtualField(self, key) -> bool:

        try:
             self.virtualFields.pop(key)
             return True
        except KeyError:
            return False

    def setVirtualField(self, key, value) -> None:

        self.virtualFields[key] = value

    def getVirtualField(self, key):
    
        return self.virtualFields[key]

    
    def getReplacementFields(self) -> dict:
    
        return self.replacementFields

    def removeReplacementField(self, key) -> bool:

        try:
             self.replacementFields.pop(key)
             return True
        except KeyError:
            return False

    def setReplacementField(self, key, value) -> None:

        self.replacementFields[key] = value

    def getReplacementField(self, key):
    
        return self.replacementFields[key]

    def generateImageResource(self, img, fileName) -> None:
        
        self.imageResources[fileName] = img

    def clearVirtualFields(self):

        self.virtualFields.clear()

    def clearReplacementFields(self):

        self.replacementFields.clear