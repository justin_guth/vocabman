from src.ExcelReader import ExcelReader
import genanki
from src.vendor.python_helpers.functionals import fold

reader = ExcelReader()

data = reader.readWorkbook("./res/lessons/spreadsheets/lesson1.xlsx")

style = """
.card {
 font-family: times;
 font-size: 30px;
 text-align: center;
 color: black;
 background-color: white;
}
hr {
    border: 1px solid black;
    border-radius: 1px;
}

.card1 { background-color: #ffb7c5}


"""

my_model = genanki.Model(
  5446843253565,
  'Simple Model',
  fields=[
    {'name': 'Hiragana'},
    {'name': 'Kanji'},
    {'name': 'German'},
  ],
  templates=[
    {
      'name': 'Card 1',
      'qfmt': '{{Hiragana}}<br />{{Kanji}}',
      'afmt': '{{FrontSide}}<hr id="german">{{German}}',
    },
  ],
  css=style
  )

vocabDeck = genanki.Deck(178115513, 'Lektion 1')

for d in data:

    if not fold(lambda a, b: a or (b != None) , False, d): # check empty lines to skip

        continue

    note = genanki.Note(model=my_model, fields=[d[0], d[1] if d[1] else "", d[2]])
    vocabDeck.add_note(note)


genanki.Package(vocabDeck).write_to_file('Lektion 1.apkg')