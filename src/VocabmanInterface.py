import os
from PIL import Image

class VocabmanInterface():

    def __init__(self, controllerInstance, templatePath) -> None:
        
        self.controllerInstance = controllerInstance
        self.cachePath = os.path.join(templatePath, "cache")

    def getVirtualFields(self) -> dict:

        return self.controllerInstance.getVirtualFields()

    def removeVirtualField(self, key) -> bool:

        return self.controllerInstance.removeVirtualField(key)

    def setVirtualField(self, key, value) -> None:

        self.controllerInstance.setVirtualField(key, value)

    def getVirtualField(self, key):
    
        return self.controllerInstance.getVirtualField(key)

    def getReplacementFields(self) -> dict:

        return self.controllerInstance.getReplacementFields()

    def removeReplacementField(self, key) -> bool:

        return self.controllerInstance.removeReplacementField(key)

    def setReplacementField(self, key, value) -> None:

        self.controllerInstance.setReplacementField(key, value)

    def getReplacementField(self, key):
    
        return self.controllerInstance.getReplacementField(key)

    def generateImageResource(self, img, fileName) -> None:
        
        self.controllerInstance.generateImageResource(img, str(fileName))

    def packageResources(self, filePath) -> None:

        self.controllerInstance.packageResources(filePath)

    def cacheResources(self) -> None:

        if not os.path.exists(self.cachePath):
            os.mkdir(self.cachePath)

        self.controllerInstance.packageResources(self.cachePath)

    def isImageCached(self, name):

        name += ".png"

        path = os.path.join(self.cachePath, name)

        if not os.path.exists(path):

            return False

        return True

    def getCachedImage(self, name):

        name += ".png"

        path = os.path.join(self.cachePath, name)

        if not os.path.exists(path):

            return None

        return Image.open(path, "r")

    def zip(self, filePath):

        self.packageResources(filePath)
        self.controllerInstance.zip(filePath)