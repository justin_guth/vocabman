from src.ExcelParser import ExcelParser
import genanki
import os
from src.vendor.python_helpers.functionals import fold
import random
import uuid
import shutil


class DeckBuilder:

    defaultModelId = 5446843253565
    defaultDeckId = 178115513

    @staticmethod
    def dump(path, vocabSets, defaultTemplate, controller, deckName=None):

        if not os.path.exists("./tmp"):
            os.mkdir("./tmp")

        name = deckName if deckName != None else " & ".join(
            list(map(lambda e: e.getTitle(), vocabSets)))
        vocabDeck = genanki.Deck(random.randint(1, 2**32 - 1), name)
        media = []

        for vocabSet in vocabSets:

            template = vocabSet.getTemplate() if vocabSet.hasCustomTemplate() else defaultTemplate

            questionFormat = template.applyBackHtml(
                vocabSet) if vocabSet.isReversed() else template.applyFrontHtml(vocabSet)
            answerFormat = template.applyFrontHtml(
                vocabSet) if vocabSet.isReversed() else template.applyBackHtml(vocabSet)

            setModel = genanki.Model(
                random.randint(1, 2**32 - 1),
                "Model",
                # template.getConfigJson()["fields"] TODO : replace with getter
                fields=[{"name": "UUID"}] + template.getConfigJson()["fields"],
                templates=[
                    {
                        'name': "Card",
                        'qfmt': questionFormat,
                        'afmt': answerFormat,
                    }
                ],
                css=template.getStyleCss()
            )

            fieldsCount = len(template.getConfigJson()["fields"])
            cut = vocabSet.getColumns() if vocabSet.getColumns() < fieldsCount else fieldsCount

            print("Creating Cards...")
            n = 0
            cardCount = len(vocabSet.getVocabs())

            for vocab in vocabSet.getVocabs():
                n += 1
                print("Creating card", n, "/", cardCount)

                controller.clearVirtualFields()
                controller.clearReplacementFields()

                if template.hasProcessor():

                    template.process(vocab, controller, path)
                    replacementFields = controller.getReplacementFields()

                    newQFormat = questionFormat
                    newAFormat = answerFormat

                    for key, value in replacementFields.items():

                        newAFormat = newAFormat.replace(
                            "{{"+key+"}}", value)
                        newQFormat = newQFormat.replace(
                            "{{"+key+"}}", value)


                    setModel = genanki.Model(
                        random.randint(1, 2**32 - 1),
                        "Model",
                        # template.getConfigJson()["fields"] TODO : replace with getter
                        fields=[{"name": "UUID"}] + template.getConfigJson()["fields"] + \
                        controller.getVirtualFieldsList(),
                        templates=[
                            {
                                'name': "Card",
                                'qfmt': newQFormat,
                                'afmt': newAFormat,
                            }
                        ],
                        css=template.getStyleCss()
                    )

                    #print("overriding model:", setModel)


                note = genanki.Note(
                    model=setModel,
                    fields=[
                        str(uuid.uuid4())] + list(map(lambda e: e if e != None else "", vocab))[:cut]
                    + controller.getVirtualFieldsValuesList()
                )
                vocabDeck.add_note(note)
            print("Done Creating Cards...")

        finalPath = path

        finalPath += ".apkg"

        apkg = genanki.Package(vocabDeck)

        #print("Setting media files:")
        #print(media) TODO check why not working
        apkg.media_files = media

        print("Writing deck...")
        apkg.write_to_file(finalPath)
        
        print("Done writing deck...")
        
