from enum import Enum
from src.vendor.config.Config import Config

class Direction(Enum):
    Forward = 1
    Reversed = 2


class VocabSet:

    def __init__(self):

        self.vocabs = []
        self.title = "Vocab set"
        self.comment = ""
        self.columns = 2
        self.direction = Direction.Forward
        self.metaData = {}
        self.customTemplate = None

    def addVocab(self, vocabData):

        self.vocabs.append(vocabData)

    def setTitle(self, title):

        self.title = title

    def getTitle(self):

        return self.title 

    def getVocabs(self):

        return self.vocabs 

    def setComment(self, comment):

        self.comment = comment

    def setColumns(self, columns):

        self.columns = columns

    def getColumns(self):

        return self.columns

    def reverse(self):

        self.direction = Direction.Reversed if self.direction == Direction.Forward else Direction.Forward

    def isReversed(self):

        return self.direction == Direction.Reversed

    def setTemplate(self, template):

        self.customTemplate = template
        conf = Config(jsonText=template.getConfigJsonText())

        if conf.defined("reversible") and conf["reversible"] == False:

            self.direction = Direction.Forward

    def hasCustomTemplate(self):

        return self.customTemplate != None

    def getTemplate(self):

        return self.customTemplate

    def unlinkTemplate(self):

        self.customTemplate = None

    def __repr__(self):

        pattern = "{:<15}: {:<30} ({direction}) [{template}]"
        directionText = "->" if self.direction == Direction.Forward else "<-"
        appliedTemplate = "d" if self.customTemplate == None else self.customTemplate.__repr__()
        return pattern.format(self.title, self.comment, direction=directionText, template=appliedTemplate)
