from src.ExcelParser import ExcelParser
import os

class FileParser:

    @staticmethod
    def parse(path):

        extention = FileParser.getExtention(path)

        if extention == ".xlsx":
            return ExcelParser.getInstance().parseExcel(path)
        elif extention == ".csv":
            raise NotImplementedError()
        else:
            raise NotImplementedError()

    @staticmethod
    def isFileValid(path):
        extention = FileParser.getExtention(path)

        if extention == ".xlsx":
            return ExcelParser.getInstance().isFileValid(path)
        elif extention == ".csv":
            raise NotImplementedError()
        else:
            return False


    @staticmethod
    def checkFileValidity(path):
        extention = FileParser.getExtention(path)

        if extention == ".xlsx":
            return ExcelParser.getInstance().checkFileValidity(path)
        elif extention == ".csv":
            raise NotImplementedError()
        else:
            raise NotImplementedError()
  
    @staticmethod
    def getExtention(path):
        return os.path.splitext(path)[1]
        