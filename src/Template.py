import os
import json
#import importlib
import imp
from src.VocabmanInterface import VocabmanInterface
import uuid


class Template:

    backsideFileName = 'back.html'
    frontsideFileName = 'front.html'
    configFileName = 'config.json'
    styleFileName = 'style.css'

    requiredFiles = [backsideFileName,
        frontsideFileName, configFileName, styleFileName]

    def __init__(self, path):

        Template.checkDirectory(path)
        self.path = path

        filesToRead = Template.requiredFiles
        setters = [self.setBackHtml, self.setFrontHtml,
            self.setConfigJson, self.setStyleCss]

        for i in range(len(filesToRead)):

            with open(os.path.join(path, filesToRead[i]), "r") as f:

                setters[i](f.read())

    def setBackHtml(self, text):
        self.backHtml = text

    def applyBackHtml(self, vocabSet):
        return self.backHtml

    def setFrontHtml(self, text):
        self.frontHtml = text

    def applyFrontHtml(self, vocabSet):
        return self.frontHtml

    def getConfigJsonText(self):

        return self.configJsonText

    def setConfigJson(self, text):
        self.configJsonText = text
        self.configJson = json.loads(text)

    def getConfigJson(self):
        return self.configJson

    def setStyleCss(self, text):
        self.styleCss = text

    def getStyleCss(self):
        return self.styleCss

    def hasProcessor(self):

        return os.path.exists(os.path.join(self.path, "processor.py"))

    def process(self, row, controller, path):

        if not self.hasProcessor():

            raise Exception("No processor set!")  # TODO create error

        processor = imp.load_source("processor", os.path.join(self.path, "processor.py"))
        processor.process(row, VocabmanInterface(controller, self.path), path)

    def __repr__(self):

        return "\"" + self.configJson["name"] + "\""

    @ staticmethod
    def isValidDirectory(path):

        files=os.listdir(path)

        for requiredFile in Template.requiredFiles:

            if requiredFile not in files:

                return False

        return True

    @ staticmethod
    def checkDirectory(path):

        if not Template.isValidDirectory(path):

            # todo
            raise Exception(
                f"Expected file named '{requiredFile}' in directory '{path}'")


if __name__ == "__main__":

    t=Template("./res/templates/example_template")
