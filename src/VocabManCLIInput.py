import readchar
import sys
import os
from src.vendor.python_helpers.functionals import fold
from colorama import Fore
from src.commands import Commands

class VocabManCLIInput:

    inputs = []
    currentInput = -1

    colorKeywords = False

    @staticmethod
    def get(prompt):

        data = None
        text = ""
        print(prompt, end="")
        sys.stdout.flush()



        while True:

            try:
                c = readchar.readkey()
            except KeyboardInterrupt:
                raise sys.exit()

            text = text.replace(Fore.CYAN, "").replace(Fore.WHITE, "")
            textLast = text

            if c == readchar.key.ENTER:

                print()
                VocabManCLIInput.inputs.append(text)
                VocabManCLIInput.currentInput = -1
                return text

            elif c == readchar.key.BACKSPACE:

                text = text[:-1] if len(text) > 0 else ""

            elif c == readchar.key.CTRL_A:

                # load previous command (Ctrl + A)
                if VocabManCLIInput.currentInput == -1 and VocabManCLIInput.inputs != []:

                    VocabManCLIInput.currentInput = len(VocabManCLIInput.inputs) - 1
                    text = VocabManCLIInput.inputs[VocabManCLIInput.currentInput]

                elif VocabManCLIInput.currentInput > 0:

                    VocabManCLIInput.currentInput -= 1
                    text = VocabManCLIInput.inputs[VocabManCLIInput.currentInput]

                elif VocabManCLIInput.currentInput == 0:

                    text = VocabManCLIInput.inputs[VocabManCLIInput.currentInput]

            elif c == readchar.key.CTRL_S:

                # load next command (Ctrl + S)

                if VocabManCLIInput.currentInput == len(VocabManCLIInput.inputs) - 1:

                    VocabManCLIInput.currentInput = -1
                    text = ""

                elif VocabManCLIInput.currentInput < len(VocabManCLIInput.inputs) - 1 and VocabManCLIInput.currentInput >= 0:

                    VocabManCLIInput.currentInput += 1
                    text = VocabManCLIInput.inputs[VocabManCLIInput.currentInput]

            elif c == readchar.key.TAB:

                lastWord = text.split(" ")[-1]
                

                potentialPath = os.path.join(os.getcwd(), lastWord)

                potentialPathName = os.path.split(potentialPath)[0]
                potentialPathFileName = os.path.split(potentialPath)[1]

                directoryList = []
                try:
                    directoryList = os.listdir(potentialPathName)
                except:
                    pass

                matches = list(filter(lambda fil: fil[:len(potentialPathFileName)] == potentialPathFileName, directoryList))

                if (len(matches) == 1):

                    text += matches[0][len(potentialPathFileName):]

                elif (len(matches) > 1):

                    print("\nMatching paths found:" + Fore.CYAN, *map(lambda e: "\"" + e + "\"", matches), Fore.WHITE)
                    print(prompt + textLast, end="")
                    sys.stdout.flush()

                    shortestLength = min(map(lambda s: len(s), matches))
                    
                    commonPrefix = ""

                    for i in range(shortestLength):

                        firstChar = matches[0][i]

                        if fold(lambda a, b: a and (b[i] == firstChar), True, matches):

                            commonPrefix += firstChar

                        else:

                            break

                    text += commonPrefix[len(potentialPathFileName):]

            else:
                
                try:
                    text += c
                except:
                    pass

            if VocabManCLIInput.colorKeywords:

                firstWord = text.split(" ")[0]
                coloredWord = firstWord
                firstWordLength = len(firstWord)

                if len(list(filter(lambda e: e.fits(text), Commands.listUseCommands()))) > 0:

                    coloredWord = Fore.CYAN + firstWord + Fore.WHITE         

                text = coloredWord + text[firstWordLength:]

            eraseLen = len(textLast)
            eraseText = "\b" * eraseLen + " " * eraseLen + "\b" * eraseLen
            print(eraseText + text, end="")
            sys.stdout.flush()