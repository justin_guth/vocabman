import openpyxl 

class ExcelReader:

    instance = None

    @staticmethod
    def getInstance():

        if ExcelReader.instance == None:

            ExcelReader.instance = ExcelReader()

        return ExcelReader.instance

    def __del__(self):

        for value in self.cache.values():

            value.close()

    def __init__(self):

        self.cache = {}

    def _openWorkbook(self, path, cache=True):

        if path in self.cache.keys():

            return self.cache[path]

        else: 

            workbook = openpyxl.load_workbook(path, read_only=True)
            
            if cache:
            
                self.cache[path] = workbook
            
            return workbook

    def readWorkbook(self, path, cache=True):

        workbook = self._openWorkbook(path, cache=cache)
        vocabSheet = workbook["vocab"]
        rows = [list(map(lambda e: e.value, row)) for row in vocabSheet]

        if not cache:

            workbook.close()

        return rows

    def readMagicNumber(self, path, cache=False):

        workbook = self._openWorkbook(path, cache=cache)
        vocabSheet = workbook["vocab"]
        result = vocabSheet.cell(column=1, row=1).value

        if not cache:

            workbook.close()

        return result

    def uncache(self, path):

        if path not in self.cache.keys():

            raise Exception("TODO @ExcelReader::uncache")

        self.cache[path].close()
        self.cache.pop(path)