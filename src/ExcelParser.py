
from src.ExcelReader import ExcelReader
from src.exceptions.FileFormatError import FileFormatError
from src.exceptions.FormattingError import FormattingError
from src.VocabSet import VocabSet
from src.vendor.python_helpers.functionals import fold
from markdown2 import Markdown

import os

class ExcelParser:

    supportedExtentions = [".xlsx"]
    magicNumber = "VOCAB_MANAGER_FORMAT"
    instance = None
    markdowner = Markdown()

    @staticmethod
    def getInstance():

        if ExcelParser.instance == None:

            ExcelParser.instance = ExcelParser()

        return ExcelParser.instance

    def __init__(self):

        self.validityCache = {}

    def parseExcel(self, path):

        extention = os.path.splitext(path)[1]

        if extention not in ExcelParser.supportedExtentions:

            raise FileFormatError(ExcelParser.supportedExtentions, extention)

        reader = ExcelReader.getInstance()
        data = reader.readWorkbook(path, cache=False)   

        result = VocabSet()

        metaData = data[0]

        expectedFormat = "VOCAB_MANAGER_FORMAT"

        if metaData[0] != expectedFormat:

            raise FormattingError("A1", f'"{expectedFormat}"', metaData[0])

        try:
            result.setTitle(metaData[1])
        except:
            raise FormattingError("A2", "string", "missing")
        
        try:
            result.setComment(metaData[2])
        except:
            raise FormattingError("A3", "string", "missing")
        
        try:
            result.setColumns(int(metaData[3]))
        except:
            raise FormattingError("A4", "integer", "missing")
        
        for d in data[1:]:

            if not fold(lambda a, b: a or (b != None) , False, d): # check empty lines to skip

                continue

            d = list(map(lambda e: str(e) if e != None else None ,d))

            # Parse markdown to html:
            d = list(map(lambda e: ExcelParser.markdowner.convert(e) if e != None else None ,d))

            result.addVocab(d)

        return result

    def isFileValid(self, path):

        if path not in self.validityCache.keys():

            return self._loadToValidityCache(path)

        lastChanged = os.path.getctime(path)
        cachedEntry = self.validityCache[path]
        cachedChangeTime = cachedEntry[1]
        
        if lastChanged != cachedChangeTime:

            return self._loadToValidityCache(path)

        return cachedEntry[0]

    def _loadToValidityCache(self, path):

        magicNumber = ExcelReader.getInstance().readMagicNumber(path, cache=False) 
        lastChanged = os.path.getctime(path)
        isValid = magicNumber == ExcelParser.magicNumber
        self.validityCache[path] = (isValid, lastChanged)
        return isValid

    def checkFileValidity(self, path):

        if not self.isFileValid(path):

            raise FormattingError(ExcelParser.magicNumber)