from src.VocabManController import VocabManController
from src.commands import Commands
from src.exceptions.ArgumentCountError import ArgumentCountError
from src.exceptions.FormattingError import FormattingError
import colorama
from colorama import Fore
from src.VocabManCLIInput import VocabManCLIInput
import traceback

class VocabManCLI:

    inputPrompt = "VocabManCLI: " + Fore.LIGHTRED_EX + "{cwd}" + Fore.WHITE + " > "
    commands = Commands.listCommands()

    
    inputMethod = VocabManCLIInput.get
    #inputMethod = input

    def __init__(self, controller):

        colorama.init()
        self.controller = controller

    def running(self):

        return self.controller.running()

    def processInput(self):

        query = VocabManCLI.inputMethod(VocabManCLI.inputPrompt.format(cwd=self.controller.getCwd()))

        for command in VocabManCLI.commands:

            if command.fits(query):

                try:
                    command.apply(query, self.controller)
                except ArgumentCountError as e:
                    print("Error:", e)
                except FormattingError as e:
                    print("Error:", e)
                except Exception as e:
                    print("Error: The command could not be processed.", e)
                    print(traceback.format_exc() )
                break

    def run(self):

        while self.running():

            self.processInput()

def launch():

    vmCli = VocabManCLI(VocabManController())

    vmCli.run()

if __name__ == "__main__":

    launch()