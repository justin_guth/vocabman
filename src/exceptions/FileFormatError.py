class FileFormatError(Exception):

    def __init__(self, expected, actual):

        self.message = "File format {a} not supported, supported file formats are {e}.".format(
            e=''.join(expected), a=actual
        )

    def __repr__(self):

        return self.message

    def __str__(self):

        return self.message