class FormattingError(Exception):

    def __init__(self, cell, expected, actual="missing"): #todo maybe change lol

        self.message = f"Formatting error cell {cell} is {actual} expected ({expected})."

    def __repr__(self):

        return self.message

    def __str__(self):

        return self.message