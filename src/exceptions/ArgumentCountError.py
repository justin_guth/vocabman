class ArgumentCountError(Exception):

    def __init__(self, expected, actual):

        self.message = "Expected {e} argument{plural} but got {a}.".format(
            e=expected, a=actual, plural="s" if expected > 1 else ""
        )

    def __repr__(self):

        return self.message

    def __str__(self):

        return self.message