from src.AppState import AppState
import os
from src.FileParser import FileParser
from src.DeckBuilder import DeckBuilder
from src.Template import Template
from src.vendor.config.Config import Config
import shutil


class VocabManController:

    defaultTemplate = Template("./res/templates/example_template") 

    def __init__(self):

        self.appState = AppState()

    def setController(self, controller):

        self.controller = controller

    def running(self):

        return self.appState.running

    def getCwd(self):

        return self.appState.cwd

    def quit(self):

        self.appState.running = False

    def changeDirectory(self, path):

        self.appState.changeDirectory(path)

    def getPickedFiles(self):

        return self.appState.getPickedFiles()

    def getSelectedTemplates(self):

        return self.appState.getSelectedTemplates()

    def linkVocabSet(self, templateId, appliedSetId):

        if not 0 < templateId <= len(self.appState.activeTemplates):

            raise Exception("Template id out of range.")

        if not 0 < appliedSetId <= len(self.appState.pickedVocabSets):

            raise Exception("Vocab set id out of range.")

        self.appState.pickedVocabSets[appliedSetId - 1].setTemplate(self.appState.activeTemplates[templateId - 1])

    def linkAllVocabSets(self, templateId):

        if not 0 < templateId <= len(self.appState.activeTemplates):

            raise Exception("Template id out of range.")


        for vocabSet in self.appState.pickedVocabSets:

            vocabSet.setTemplate(self.appState.activeTemplates[templateId - 1])

    def linkUnsetVocabSets(self, templateId):

        if not 0 < templateId <= len(self.appState.activeTemplates):

            raise Exception("Template id out of range.")


        for vocabSet in self.appState.pickedVocabSets:

            if vocabSet.hasCustomTemplate():

                continue

            vocabSet.setTemplate(self.appState.activeTemplates[templateId - 1])

    def unlinkVocabSet(self, appliedSetId):

        if not 0 < appliedSetId <= len(self.appState.pickedVocabSets):

            raise Exception("Vocab set id out of range.")

        self.appState.pickedVocabSets[appliedSetId - 1].unlinkTemplate()

    def unlinkAll(self):

        for vocabSet in self.appState.pickedVocabSets:

            vocabSet.unlinkTemplate()

    def pick(self, path):

        joined = os.path.join(self.appState.cwd, path)
        
        if os.path.isfile(joined):

            self.appState.pickedVocabSets.append(FileParser.parse(joined))
           
        elif os.path.isdir(joined):

            self.appState.activeTemplates.append(Template(joined))

        else:
            #todo Make custom error class
            raise Exception("The provided argument could not be resolved to a file.")

    def pickAll(self):

        allPaths = os.listdir(self.appState.cwd)
        
        for path in allPaths:

            joined = os.path.join(self.appState.cwd, path)

            if os.path.isfile(joined):

                if not FileParser.isFileValid(joined):

                    continue

                self.appState.pickedVocabSets.append(FileParser.parse(joined))
            
            elif os.path.isdir(joined):

                if not Template.isValidDirectory(joined):

                    continue

                self.appState.activeTemplates.append(Template(joined))

            else:
                
                pass

    def deploy(self, path):

        if not os.path.isdir(path):
            
            raise Exception("Path could not be resolved")
        
        file_names = os.listdir(path)
    
        for file_name in file_names:

            shutil.move(os.path.join(path, file_name), 
                os.path.join(os.path.expandvars("%appdata%/Anki2/User 1/collection.media")))
                
        os.rmdir(path)

    def drop(self, number):

        index = number - 1

        if index < 0 or index >= len(self.appState.pickedVocabSets):

            #todo make custom error class
            raise Exception("Invalid index provided")

        return self.appState.pickedVocabSets.pop(index)

    def dropTemplate(self, number):

        index = number - 1

        if index < 0 or index >= len(self.appState.activeTemplates):

            #todo make custom error class
            raise Exception("Invalid index provided")

        return self.appState.activeTemplates.pop(index)

    def dropall(self):

        self.appState.pickedVocabSets.clear()

    def dropAllTemplates(self):

        self.appState.activeTemplates.clear()

    def dropMultiple(self, nums):

        indices = list(map(lambda e: e - 1, nums))

        pickedVocabSetsMap = {}
        index = 0
        removed = []

        for vocabSet in self.appState.pickedVocabSets:

            pickedVocabSetsMap[index] = vocabSet
            index += 1
        
        for dropIndex in indices:

            removed.append(pickedVocabSetsMap.pop(dropIndex))

        self.appState.pickedVocabSets.clear()

        for remainingKey in sorted(pickedVocabSetsMap.keys()):

            self.appState.pickedVocabSets.append(pickedVocabSetsMap[remainingKey])

        return removed

    def dropMultipleTemplates(self, nums):

        indices = list(map(lambda e: e - 1, nums))

        pickedTemplates = {}
        index = 0
        removed = []

        for vocabSet in self.appState.activeTemplates:

            pickedTemplates[index] = vocabSet
            index += 1
        
        for dropIndex in indices:

            removed.append(pickedTemplates.pop(dropIndex))

        self.appState.activeTemplates.clear()

        for remainingKey in sorted(pickedTemplates.keys()):

            self.appState.activeTemplates.append(pickedTemplates[remainingKey])

        return removed

    def checkReversible(self, vocabSet):

        if not self.isReversible(vocabSet):

            raise Exception("The deck could not be reversed because the associated template was marked as non reversible.")

    def isReversible(self, vocabSet):

        if vocabSet.hasCustomTemplate():

            template = vocabSet.getTemplate()
            c = Config(jsonText=template.getConfigJsonText())

            if c.defined("reversible") and c["reversible"] == False:

                return False
        
        return True

    def reverse(self, number):

        index = number - 1

        if index < 0 or index >= len(self.appState.pickedVocabSets):

            #todo make custom error class
            raise Exception("Invalid index provided")

        self.checkReversible(self.appState.pickedVocabSets[index])
        
        self.appState.pickedVocabSets[index].reverse()
        return self.appState.pickedVocabSets[index]

    def reverseAll(self):

        for vocabSet in self.appState.pickedVocabSets:

            if self.isReversible(vocabSet):

                vocabSet.reverse()

    def dump(self, path):

        name = None

        if self.appState.isTitleSet():

            name = self.appState.getTitle()

        DeckBuilder.dump(path, self.appState.pickedVocabSets, VocabManController.defaultTemplate, self, deckName=name)


    def title(self, title):

        self.appState.setTitle(title)

    def getTitle(self):

        return self.appState.getTitle()

    def isTitleSet(self):

        return self.appState.isTitleSet()

    def hacf(self):

        self.appState.running = False

    def getVirtualFields(self) -> dict:

        return self.appState.getVirtualFields()

    def getVirtualFieldsList(self) -> dict:

        virtualFields = self.appState.getVirtualFields()
        result = []

        for key in sorted(virtualFields.keys()):

            result.append({"name": key})

        return result

    def getVirtualFieldsValuesList(self) -> dict:

        virtualFields = self.appState.getVirtualFields()
        result = []

        for key in sorted(virtualFields.keys()):

            result.append(virtualFields[key])

        return result

    def clearVirtualFields(self):

        self.appState.clearVirtualFields()

    def clearReplacementFields(self):

        self.appState.clearReplacementFields()


    def removeVirtualField(self, key) -> bool:

        return self.appState.removeVirtualField(key)

    def setVirtualField(self, key, value) -> None:

        self.appState.setVirtualField(key, value)

    def getVirtualField(self, key):
    
        return self.appState.getVirtualField(key)

    def getReplacementFields(self) -> dict:

        return self.appState.getReplacementFields()

    def removeReplacementField(self, key) -> bool:

        return self.appState.removeReplacementField(key)

    def setReplacementField(self, key, value) -> None:

        self.appState.setReplacementField(key, value)

    def getReplacementField(self, key):
    
        return self.appState.getReplacementField(key)
    
    def generateImageResource(self, img, fileName) -> None:
        
        self.appState.generateImageResource(img, fileName)

    def packageResources(self, filePath) -> None:

        if not os.path.isdir(filePath):
            
            os.mkdir(filePath)

        for name, img in self.appState.imageResources.items():

            img.save(os.path.join(filePath, name + ".png"), "PNG")

    def zip(self, filePath):

        if not os.path.isdir(filePath):

            raise Exception(f"Folder '{filePath}' does already exist.")

        shutil.make_archive(filePath, filePath + ".zip")