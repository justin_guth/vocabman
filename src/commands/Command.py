from src.exceptions.ArgumentCountError import ArgumentCountError

class Command:

    def expectArguments(self, expectedNumber, query):

        querySplit = query.split(" ")
        actual = len(querySplit)

        if actual != expectedNumber + 1:

            raise ArgumentCountError(expectedNumber, actual)
        
        return querySplit[1:]

    def expectMultipleArguments(self, query):

        querySplit = query.split(" ")
        
        if len(querySplit) <= 1:

            #todo set custom exception
            raise Exception()
        
        return querySplit[1:]