import os
from src.commands.Command import Command

class Title(Command):

    codes = ["title"]

    def fits(self, query):

        querySplit = query.split(" ")
         
        return querySplit[0] in Title.codes

    def apply(self, query, controller):

        arg = query[6:]

        controller.title(arg)

    def getHelpText(self):

        return ("Sets the deck title.", Title.codes)
