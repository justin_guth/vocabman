from src.commands.Command import Command

class Pick(Command):

    codes = ["pick", "take"]

    def __init__(self):

        pass

    def fits(self, query):

        querySplit = query.split(" ")
        return querySplit[0] in Pick.codes
    
    def apply(self, query, controller):

        args = self.expectArguments(1, query)

        controller.pick(args[0])

    def getHelpText(self):
    
        return ("Picks a vocab or template file.", Pick.codes)