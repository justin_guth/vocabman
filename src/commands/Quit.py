class Quit:

    codes = ["quit", "exit", "halt"]

    def __init__(self):

        pass

    def fits(self, query):

        return query in Quit.codes

    def apply(self, query, controller):

        controller.quit()

    def getHelpText(self):

        return ("Quits the application.", Quit.codes)
