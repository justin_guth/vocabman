from src.commands.Command import Command

class Link(Command):

    codes = ["template", "link"]

    def __init__(self):

        pass

    def fits(self, query):

        querySplit = query.split(" ")
        return querySplit[0] in Link.codes
    
    def apply(self, query, controller):

        args = self.expectMultipleArguments(query)

        templateId = int(args[0])

        args2 = args[1:]

        if "all" in args2:

            controller.linkAllVocabSets(templateId)

        elif "unset" in args2:

            controller.linkUnsetVocabSets(templateId)

        else:

            appliedSets = list(map(int, args2))

            for appliedSet in appliedSets:

                controller.linkVocabSet(templateId, appliedSet)

    def getHelpText(self):
    
        return ("Assigns a template to a vocab set.", Link.codes)