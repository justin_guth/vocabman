import os
from colorama import Fore, Back, Style
from src.FileParser import FileParser
from src.Template import Template

class ListDirectory:

    codes = ["ls"]

    def fits(self, query):

        return query in ListDirectory.codes

    def apply(self, query, controller):

        cwd = controller.getCwd()

        contents = os.listdir(controller.getCwd())

        folders = list(map(
            lambda path: (path, Template.isValidDirectory(path)),
            filter(
                lambda e: os.path.isdir(os.path.join(cwd, e)), 
                contents
            )
        ))

        files = list(map(lambda path: (path, FileParser.isFileValid(path)) ,filter(lambda e: os.path.isfile(os.path.join(cwd, e)), contents)))

        folderOutput = Style.DIM + Fore.GREEN 

        for (folder, isValid) in folders:

            highlight = Style.BRIGHT if isValid else Style.DIM
            prefix = "   t" if isValid else "   #"

            folderOutput += highlight + prefix + "> "  + folder + "\n"
        
        fileOuput = Fore.YELLOW

        for (fileName, isValid) in files:
            
            highlight = Style.BRIGHT if isValid else Style.DIM
            prefix = "   v" if isValid else "   *"
            fileOuput += highlight + prefix + "> " + fileName + "\n" + Style.DIM
        
        print(Style.BRIGHT + Fore.CYAN + "\nListing contents of \"" + cwd + "\":\n")
        print(folderOutput + fileOuput + Fore.WHITE + Style.BRIGHT)

    def getHelpText(self):

        return ("Lists the contents of the current directory.", ListDirectory.codes)
