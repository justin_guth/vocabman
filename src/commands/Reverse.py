from src.commands.Command import Command
from colorama import Fore

class Reverse(Command):

    codes = ["reverse"]

    def __init__(self):

        pass

    def fits(self, query):

        querySplit = query.split(" ")
        return querySplit[0] in Reverse.codes
    
    def apply(self, query, controller):

        args = self.expectMultipleArguments(query)
        
        if "all" in args:

            controller.reverseAll()
            print("Reversed all applicable vocab sets.") 

        else:

            for num in args:
                
                print("Reversed vocab set: " + Fore.CYAN + "\"", controller.reverse(int(num)), "\"" + Fore.WHITE, sep="")

    def getHelpText(self):
    
        return ("Reverses the picked vocab set at the provided position", Reverse.codes)