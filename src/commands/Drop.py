from src.commands.Command import Command
from colorama import Fore

class Drop(Command):

    codes = ["drop"]

    def __init__(self):

        pass

    def fits(self, query):

        querySplit = query.split(" ")
        return querySplit[0] in Drop.codes
    
    def apply(self, query, controller):

        args = self.expectMultipleArguments(query)

        if "all" in args:

            controller.dropall()
            print("Dropped all vocab sets.") 

        elif len(args) == 1:
            
            print("Dropped vocab set: " + Fore.CYAN + "\"", controller.drop(int(args[0])), "\"" + Fore.WHITE, sep="")
        
        else:

            nums = list(map(int, args))

            droppedSets = controller.dropMultiple(nums)

            print("Dropped vocab sets:")

            for droppedSet in droppedSets:

                print("\t- " + Fore.CYAN + "\"", droppedSet, "\"" + Fore.WHITE, sep="")

    def getHelpText(self):
    
        return ("Drops the picked vocab set at the provided position", Drop.codes)