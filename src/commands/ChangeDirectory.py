import os
from src.commands.Command import Command

class ChangeDirectory(Command):

    codes = ["cd"]

    def fits(self, query):

        querySplit = query.split(" ")
         
        return querySplit[0] in ChangeDirectory.codes

    def apply(self, query, controller):

        args = self.expectArguments(1, query)

        controller.changeDirectory(args[0])

    def getHelpText(self):

        return ("Changes directory.", ChangeDirectory.codes)
