from src.commands.Command import Command
from colorama import Fore

class Deploy(Command):

    codes = ["deploy", "launch", "media"]

    def __init__(self):

        pass

    def fits(self, query):

        querySplit = query.split(" ")
        return querySplit[0] in Deploy.codes
    
    def apply(self, query, controller):

        args = self.expectArguments(1, query)

        print("Deploying folder " + Fore.CYAN + "\"",args[0], "\"" + Fore.WHITE, sep="")
        controller.deploy(args[0])

    def getHelpText(self):
    
        return ("Deploys folder contents to media file", Deploy.codes)