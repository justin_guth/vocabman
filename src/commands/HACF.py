from src.commands.Command import Command
from colorama import Fore
from time import sleep
from string import hexdigits
import random

class HACF(Command):

    codes = ["HACF", "hacf"]

    def __init__(self):

        pass

    def fits(self, query):

        return query in HACF.codes
    
    def apply(self, query, controller):

        print(Fore.YELLOW + "Beep beep...")
        sleep(3)
        print(Fore.RED + "BrrzZrzZzzssz...")   
        sleep(2)
        
        for i in range(100):

            symbols = hexdigits + "#+*~-_§$%&()!"

            for k in range(4):

                symbolChain = ""

                for n in range(5):

                    symbolChain += random.choice(symbols)

                print(random.choice(["ERROR", "FIRE#", "BEEEP", "ZZZZZ"] + [symbolChain] * 10), end="")

                for j in range(20):

                    print (random.choice(symbols), end = "")

            print()

        print(Fore.WHITE)
        controller.hacf()

    def getHelpText(self):
    
        return None