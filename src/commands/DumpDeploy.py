from src.commands.Command import Command
from colorama import Fore
import os

class DumpDeploy(Command):

    codes = ["dumpdeploy", "dd"]

    def __init__(self):

        pass

    def fits(self, query):

        querySplit = query.split(" ")
        return querySplit[0] in DumpDeploy.codes
    
    def apply(self, query, controller):

        args = self.expectArguments(1, query)

        print("Dumping vocab sets into " + Fore.CYAN + "\"",args[0], "\"" + Fore.WHITE, sep="")
        controller.dump(args[0])
         
        if os.path.exists(args[0]+".res"):

            print("Deploying folder " + Fore.CYAN + "\"",args[0], "\"" + Fore.WHITE, sep="")
            controller.deploy(args[0]+".res")

        else:

            print("Error:", args[0]+".res", "does not exist.")

    def getHelpText(self):
    
        return ("Dumps all picked vocab entries into an apkg file and deploys media files", DumpDeploy.codes)