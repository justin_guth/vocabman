from src.commands.Command import Command
from colorama import Fore

class Dump(Command):

    codes = ["dump", "pack"]

    def __init__(self):

        pass

    def fits(self, query):

        querySplit = query.split(" ")
        return querySplit[0] in Dump.codes
    
    def apply(self, query, controller):

        args = self.expectArguments(1, query)

        print("Dumping vocab sets into " + Fore.CYAN + "\"",args[0], "\"" + Fore.WHITE, sep="")
        controller.dump(args[0])
       

    def getHelpText(self):
    
        return ("Dumps all picked vocab entries into an apkg file", Dump.codes)