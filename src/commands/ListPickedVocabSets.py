class ListPickedVocabSets:

    codes = ["list", "picked"]

    def __init__(self):

        pass

    def fits(self, query):

        return query in ListPickedVocabSets.codes

    def apply(self, query, controller):

        files = controller.getPickedFiles()
        templates = controller.getSelectedTemplates()

        titleOutput = "No title set."

        if controller.isTitleSet():

            titleOutput = "Title:\t" + controller.getTitle()


        print(titleOutput)

        print("Selected vocab sets:")

        for i in range(len(files)):

            print("   ", i + 1, " : ", files[i], sep="")

        print()
        print("Selected Templates:")

        for i in range (len(templates)):

            print("   ", i + 1, " : ", templates[i], sep="")

    def getHelpText(self):

        return ("Lists all files you picked up.", ListPickedVocabSets.codes)
