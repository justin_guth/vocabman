from src.commands.Command import Command
from colorama import Fore

class DropTemplate(Command):

    codes = ["dropt"]

    def __init__(self):

        pass

    def fits(self, query):

        querySplit = query.split(" ")
        return querySplit[0] in DropTemplate.codes
    
    def apply(self, query, controller):

        args = self.expectMultipleArguments(query)

        if "all" in args:

            controller.dropAllTemplates()
            print("Dropped all templates.") 

        elif len(args) == 1:
            
            print("Dropped template: " + Fore.CYAN, controller.dropTemplate(int(args[0])), Fore.WHITE, sep="")
        
        else:

            nums = list(map(int, args))

            droppedSets = controller.dropMultipleTemplates(nums)

            print("Dropped templates:")

            for droppedTemplate in droppedTemplates:

                print("\t- " + Fore.CYAN + "\"", droppedTemplate, "\"" + Fore.WHITE, sep="")

    def getHelpText(self):
    
        return ("Drops the template set at the provided position", DropTemplate.codes)