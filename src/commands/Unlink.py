from src.commands.Command import Command

class Unlink(Command):

    codes = ["untemplate", "unlink"]

    def __init__(self):

        pass

    def fits(self, query):

        querySplit = query.split(" ")
        return querySplit[0] in Unlink.codes
    
    def apply(self, query, controller):

        args = self.expectMultipleArguments(query)
        
        if "all" in args:

            controller.unlinkAll()

        else:

            appliedSets = list(map(int, args))

            for appliedSet in appliedSets:

                controller.unlinkVocabSet(appliedSet)

    def getHelpText(self):
    
        return ("clears a custom template from a vocab set.", Unlink.codes)