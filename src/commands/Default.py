class Default:

    message = "Command not recognised: \"{msg}\""

    def __init__(self):

        pass

    def fits(self, query):

        return True
    
    def apply(self, query, controller):

        #todo do custom Exception 
        raise Exception(Default.message.format(msg=query))

    def getHelpText(self):
    
        return None