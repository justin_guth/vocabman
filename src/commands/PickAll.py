from src.commands.Command import Command

class PickAll(Command):

    codes = ["pickall", "takeall"]

    def __init__(self):

        pass

    def fits(self, query):

        querySplit = query.split(" ")
        return querySplit[0] in PickAll.codes
    
    def apply(self, query, controller):

        controller.pickAll()

    def getHelpText(self):
    
        return ("Picks a vocab or template file.", PickAll.codes)