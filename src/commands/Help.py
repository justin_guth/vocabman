from src.commands import Commands
from src.vendor.python_helpers.functionals import fold

class Help:

    codes = ["help"]

    def __init__(self):

        pass

    def fits(self, query):

        queryComponents = query.split(" ")
        return queryComponents[0] in Help.codes 

    def listHelp(self, commandNames=None):

        if commandNames != None:

            raise NotImplementedError("This functionality is yet to be implemented.")

        result = ""
        for command in Commands.listCommands():

            helpInfo = command.getHelpText()
            
            if helpInfo == None:

                continue

            row = "{:<25}: {:<50}"
            comms = fold(lambda a, b: a + b + ", ", "", helpInfo[1])[:-2]
            info = helpInfo[0]
            result += row.format(comms, info) + "\n"

        print(result)

    def apply(self, query, controller):

        queryComponents = query.split(" ")
        
        if len(queryComponents) == 1:

            self.listHelp()

        else:

            self.listHelp(queryComponents[1:]) 

    def getHelpText(self):

        return ("Lists all commands and their functionality.", Help.codes)