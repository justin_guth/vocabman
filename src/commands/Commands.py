from src.commands.Quit import Quit
from src.commands.Default import Default
from src.commands.Help import Help
from src.commands.ListDirectory import ListDirectory
from src.commands.ChangeDirectory import ChangeDirectory
from src.commands.Empty import Empty
from src.commands.ListPickedVocabSets import ListPickedVocabSets
from src.commands.Pick import Pick
from src.commands.Drop import Drop
from src.commands.Dump import Dump
from src.commands.Reverse import Reverse
from src.commands.HACF import HACF
from src.commands.DropTemplate import DropTemplate
from src.commands.Link import Link
from src.commands.Unlink import Unlink
from src.commands.PickAll import PickAll
from src.commands.Title import Title
from src.commands.Deploy import Deploy
from src.commands.DumpDeploy import DumpDeploy

def listCommands():

    return [
        Empty(),
        Dump(),
        Drop(),
        DropTemplate(),
        Reverse(),
        Link(),
        Unlink(),
        Title(),
        Pick(),
        PickAll(),
        DumpDeploy(),
        Deploy(),
        ListPickedVocabSets(),
        ChangeDirectory(),
        ListDirectory(),
        Help(),
        Quit(),
        HACF(),
        Default()
    ]

def listUseCommands():

    return [
        Dump(),
        Drop(),
        DropTemplate(),
        Reverse(),
        Link(),
        Unlink(),
        Title(),
        Pick(),
        Deploy(),
        DumpDeploy(),
        PickAll(),
        ListPickedVocabSets(),
        ChangeDirectory(),
        ListDirectory(),
        Help(),
        Quit(),
    ]