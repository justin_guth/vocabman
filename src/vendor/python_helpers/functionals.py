def zipWith(func, a, b, *args):

    shortestLength = min(len(a), len(b))

    for l in args:

        shortestLength = min(shortestLength, len(l))
    
    return [func(a[i], b[i], *map(lambda e: e[i],args)) for i in range(shortestLength)]

def fold(func, neutral, l):
    
    result = neutral

    if (len(l) == 0):

        return result

    first = None
    rest = l

    while rest != []:

        first = rest[0]
        rest = rest[1:]

        result = func(result, first)
    
    return result

def takeWhile(predicate, l):

    result = []

    for e in l:

        if not predicate(e):

            break

        result += e

    return result


    
if __name__ == "__main__":

    def print3(a, b, c):

        print("a:", a, "b:", b, "c:", c)

    lists = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]

    ## print(zipWith(lambda a, b, c: a*b*c, *lists))

    print(fold(lambda a, b: a + b, [], lists))